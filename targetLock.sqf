params ["_veh", "_tar"];

_veh reveal [_tar, 4];
_veh lookat _tar;
sleep 3;
_veh forgetTarget _tar;

while {({alive _x} count (crew _tar)) > 0} do
{
	_veh reveal [_tar, 4];
	_veh lookat _tar;
	sleep 0.3;
	_veh forgetTarget _tar;
};